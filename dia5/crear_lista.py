'''
Crea una función que reciba como parámetro una lista vacía y un entero con el número de valores aleatorios a crear
y rellene la lista de dichos valores

Crea otra función que reciba como parámetro una lista con 50 números aleatorios enteros comprendidos entre 0 y 99,
que elimine todos lo números impares de la lista. La función no debe devolver nada.

Imprime la lista antes y después de pasársela a la función de eliminar impares.
'''

import random


def generate_random_numbers(numbers_list:list, items=50):
    for _ in range(items):
        numbers_list.append(int(random.random() * 100))
    return numbers_list


def drop_even(numbers_list:list):
    # El rebanado de la lista crea una copia del elemento, por lo que sí se puede editar "en caliente",
    for number in numbers_list[:]:
        if number % 2 != 0:
            numbers_list.remove(number)


numbers = []
numbers = generate_random_numbers(numbers, )
print(numbers)
drop_even(numbers)
print(numbers)



