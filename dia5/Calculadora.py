class Calculadora:
    def __init__(self, number_1: int, number_2: int):
        self.__number_1 = number_1
        self.__number_2 = number_2

    def sumar(self):
        return self.__number_1 + self.__number_2

    def restar(self):
        return self.__number_1 - self.__number_2

    def dividir(self):
        return self.__number_1 / self.__number_2

    def multiplicar(self):
        return self.__number_1 * self.__number_2

    def update_numbers(self, number_1=None, number_2=None):
        if number_1 is not None:
            self.__number_1 = number_1
        if number_2 is not None:
            self.__number_2 = number_2

    def ver_valores(self):
        return f"El valor del primer número es {str(self.__number_1)} y el del segundo {str(self.__number_2)}"

    # Opción para devolver los valores, de forma que se pueden utilizar independientemente fuera de la clase
    def get_valores(self):
       return self.__number_1, self.__number_2


operaciones = Calculadora(2,4)

print(operaciones.ver_valores())

num1, num2 = operaciones.get_valores()
print(f"El valor del primer número es {num1} y el del segundo es {num2}")

print(f"la suma es {operaciones.sumar()}")
print(f"la resta es {operaciones.restar()}")
print(f"la multiplicación es {operaciones.multiplicar()}")
print(f"la división es {operaciones.dividir()}")
print(f"Actualiza los valores: num1 = 5, num2 = 6")
operaciones.update_numbers(5,6)
print(operaciones.ver_valores())
print(f"Actualiza los valores: num2 = 8")
operaciones.update_numbers(number_2=8)
print(operaciones.ver_valores())
print(operaciones.get_valores()) # Este método devuelve una tupla

