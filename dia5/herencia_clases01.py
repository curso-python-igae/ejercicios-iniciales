class Reverse_String:
    
    def __init__(self, frase):
        self.__frase = frase
        
    def get_frase_inversa(self):
        return self.__frase.split()
        

frase_inversa = Reverse_String("Mi diario Python")
print(frase_inversa.get_frase_inversa())

