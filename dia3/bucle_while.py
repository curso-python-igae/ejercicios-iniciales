'''
Escribir un programa que muestre el eco de todo
lo que el usuario escriba por pantalla
hasta que escriba 'salir'
'''

while True:
	word = input ('Escribe algo (Salir para terminar) > ')
	if (word.lower() == 'salir'):
		print('Saliendo...')
        break
    # No es necesario hacer un else, puesto que en caso de coincidir saldría directamente
    #else:       
	#	print(word)
    print(word) # corrección profesor