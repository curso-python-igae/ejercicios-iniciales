'''
Crear una pirámide de números, desde un número dado hasta 1,
de forma descendente y mostrando únicamente los impares
'''


num = int(input("Introduce un número: "))

for i in range (num):
    mod = i % 2
    if mod != 0:
        for j in range (i, 0, -2):
            print(j, end = " " ) # esto establece el caracter final de la línea, para 
        print("")
    
# SOLUCION PROPUESTA

n = int(input("Introduce la altura del triángulo (entero positivo): "))
for i in range(1, n+1, 2):
    for j in range(i, 0, -2):
        print(j, end=" ")
    print("")