'''
Escribir una función que calcule el área de un círculo (A = pi*r2) y otra que calcule el
volumen de un cilindro (v=pi*r2*h), utilizando la primera función.
'''
import math

def get_area(r=0):
    area = round((math.pi*(r**2)),2)
    return area
    
def get_volumen(r=0, h=0):
    area = get_area(r)
    volumen = round((area*h),2)
    return volumen
    
radio = int(input("Introduce el radio del cilindro (en cm): "))
altura = int(input("Introduce la altura del cilindro (en cm): "))

print(f"El área del círculo es {get_area(radio)} cm2 y el volumen es {get_volumen(radio,altura)} cm3")