'''
Con un diccionario de precios y frutas, preguntar al usuario
qué fruta y números de kilos desea comprar. Si la fruta no está
en el diccionario debemos devolver un error y en caso contrario 
el precio de la compra
'''

precios = {
    'platano' : 1.35,
    'manzana' : 0.80,
    'pera' : 0.85,
    'naranja' : 0.70
}

fruta = input("Indica la fruta que quieres comprar: ").encode('raw_unicode_escape').decode('utf-8')


if fruta.lower() not in precios:
    print(f"No disponemos ahora mismo de {fruta}")
else:
    kilos = input("¿Cuántos kilos quieres?: ")
    if not kilos.isdecimal() or int(kilos) <= 0:
        print("La cantidad de kilos debe ser superior a 0")
    else:
        valor_compra = float(kilos)*precios[fruta]
        print(f"El precio de la compra es {valor_compra}")
    
