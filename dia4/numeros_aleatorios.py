'''
Crear una función que genere lista con 50 números aleatorios
enteros comprendidos entre 0 y 99, que imprima el valor más alto,
el más bjo y la media de todos los valores.
La función debe devolver la lista de valores generados.
'''

import random

def generar_aleatorios(elementos=1):
    numeros = []
    for i in range(elementos):
        aleatorio = int(random.random()*100)
        if len(numeros) == 0 or aleatorio not in numeros:
            numeros.append(aleatorio)
    print(f"El valor más alto de la lista es {max(numeros)}")
    print(f"El valor más bajo de la lista es {min(numeros)}")
    print(f"La media de los valores de la lista es {sum(numeros)/len(numeros)}")
    return numeros

lista_de_numeros = generar_aleatorios(4)    
print(f"La lista de números aleatorios es {lista_de_numeros}")
