'''
Pedir el número de dni y devolver la letra correspondiente
'''

LETRAS = ['T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E']

dni = input("Introduce tu número de dni: ")

if len(dni) != 8 and not dni.isdecimal():
    print("El dni introducido no es correcto")
else:
    resto = int(dni) % 23
    print (f"La letra de tu dni es {LETRAS[resto]}")
    
 