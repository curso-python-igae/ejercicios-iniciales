'''
dividir los alumnos en dos grupos:
grupo A mujeres con nombres anterior a la M y hombres con nombre posterior a la N
grupo B, el resto.
'''

nombre = input("Introduce tu nombre: ")
sexo = input("Indica tu sexo (Mujer -> M/ Hombre -> H): ")

if (sexo == "M") and (nombre[0].upper() < 'M'):
    grupo = "A"
elif (sexo == "H") and (nombre[0].upper() > 'N'):
    grupo = "A"
else:
    grupo = "B"
    
print(f"Estás en el grupo {grupo}")    