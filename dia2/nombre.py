'''
Pedir el nombre al usuario, y mostrarlo por pantalla en mayúsculas,
indicando el número de letras
'''

nombre = input("Indica tu nombre: ").upper()

print (f"Hola {nombre}. Tu nombre tiene {len(nombre)} letras")
print ("Hola {}. Tu nombre tiene {} letras".format(nombre, len(nombre)))
print ("Hola",nombre,". Tu nombre tiene ",len(nombre)," letras")
print ("Hola " + nombre + ". Tu nombre tiene " + str(len(nombre))  + " letras")