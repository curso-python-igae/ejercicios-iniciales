'''
Escribir un programa en python que calcule el IMC (Índice de Masa Corporal)
a partir del peso y la altura pedido al usuario
'''

peso = input("Indica tu peso en kg: ")
altura = input("Indica tu altura en m: ")
imc = round(int(peso)/(float(altura)**2),2)
print(f"Tu índice de masa corporal (IMC) es {imc}")